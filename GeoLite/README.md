# Installation

## Windows

* Update GeoLiteCountry (GeoIP)
	1. Download http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz
	2. Extract it to this directory

* Install GeoLiteCity
	1. Download http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
	2. Extract it to this directory	
	
## Linux/Mac

* Update GeoLiteCountry (GeoIP)

```	
	wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz
	gzip -d GeoIP.dat.gz
```

* Install GeoLiteCity

```
	wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
	gzip -d GeoLiteCity.dat.gz
```

