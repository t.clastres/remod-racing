![alt logo](https://sauer-racing.net/favicon-96x96.png) ![build status](https://gitlab.com/t.clastres/remod-racing/badges/master/build.svg)
# Modular remod-based racing server mod originally made for https://sauer-racing.net
## Installation
```
git clone https://gitlab.com/t.clastres/remod-racing.git
cd remod-racing
git submodule init
git submodule update
cd src
make
```

## Implemented features
  * coming


## Work in progress
  * See TODO.txt

## Supported platforms
  * Mac (cmake, Xcode)
  * FreeBSD 
  * Linux
  * Windows

## Contacts
  * IRC: irc://irc.gamesurge.net/racing-xtreme
